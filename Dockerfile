FROM debian:11

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
        nano \
        vsftpd \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV LOG_FILE=/var/log/vsftpd.log \
    SSL=false \
    PASV_MIN_PORT=30000 \
    PASV_MAX_PORT=30100

RUN mkdir -p /etc/vsftpd /var/run/vsftpd/empty

COPY *.conf /etc/vsftpd/

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

WORKDIR /etc/vsftpd

EXPOSE 21/tcp

ENTRYPOINT ["/entrypoint.sh"]
CMD ["vsftpd", "/etc/vsftpd/vsftpd.conf"]
