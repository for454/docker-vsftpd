#!/bin/sh

init() {
	#add at least one user 
	if [ -z "$USERS" ]; then
		USERS="ftp|ftp"
	fi

	if [ -z "$FTP_ROOT" ]; then
		FTP_ROOT="/srv/ftp/"
	fi
	mkdir -p "$FTP_ROOT"

	if [ -z "$LOG_FILE" ]; then
		LOG_FILE="/var/log/vsftpd.log"
	fi

	if [ -z "$PASV_ENABLE" ]; then
		PASV_ENABLE=yes
	fi

	if [ -z "$PASV_MIN_PORT" ]; then
		PASV_MIN_PORT=21000
	fi

	if [ -z "$PASV_MAX_PORT" ]; then
		PASV_MAX_PORT=21010
fi
}

add_user() {
for user in $USERS ; do
	USERNAME=$(echo $user | cut -d'|' -f1)
	PASSWORD=$(echo $user | cut -d'|' -f2)
	HOME=$(echo $user | cut -d'|' -f3)

	if [ -z "$HOME" ]; then
		HOME="/home/$USERNAME"
	fi

	useradd -m -d $HOME -p $PASSWORD -s /sbin/nologin $USERNAME
done
}

write_config_file() {
	if [ ${FAMILY} == "ipv4" ]; then
		echo "listen=YES" >> /etc/vsftpd/vsftpd.conf
		echo "listen_ipv6=NO" >> /etc/vsftpd/vsftpd.conf
	else
		echo "listen=NO" >> /etc/vsftpd/vsftpd.conf
		echo "listen_ipv6=YES" >> /etc/vsftpd/vsftpd.conf
	fi

	if [ ${ANONYMOUS} == "rw" ]; then
		echo "anonymous_enable=YES" >> /etc/vsftpd/vsftpd.conf
		echo "no_anon_password=YES" >> /etc/vsftpd/vsftpd.conf
		echo "write_enable=YES" >> /etc/vsftpd/vsftpd.conf
		echo "anon_upload_enable=YES" >> /etc/vsftpd/vsftpd.conf
		echo "anon_umask=0000" >> /etc/vsftpd/vsftpd.conf
		echo "anon_root=${FTP_ROOT}" >> /etc/vsftpd/vsftpd.conf
	elif [ ${ANONYMOUS} == "ro" ]; then
		echo "anonymous_enable=YES" >> /etc/vsftpd/vsftpd.conf
		echo "no_anon_password=YES" >> /etc/vsftpd/vsftpd.conf
		echo "write_enable=NO" >> /etc/vsftpd/vsftpd.conf
		echo "anon_upload_enable=NO" >> /etc/vsftpd/vsftpd.conf
		echo "anon_umask=0022" >> /etc/vsftpd/vsftpd.conf
		echo "anon_root=${FTP_ROOT}" >> /etc/vsftpd/vsftpd.conf
	else
		echo "anonymous_enable=NO" >> /etc/vsftpd/vsftpd.conf
		echo "no_anon_password=NO" >> /etc/vsftpd/vsftpd.conf
		echo "write_enable=NO" >> /etc/vsftpd/vsftpd.conf
		echo "anon_upload_enable=NO" >> /etc/vsftpd/vsftpd.conf
	fi

	if [ ${ASCII_ENABLE} == "yes" ]; then
		echo "ascii_download_enable=yes" >> /etc/vsftpd/vsftpd.conf
		echo "ascii_upload_enable=yes" >> /etc/vsftpd/vsftpd.conf
	fi

	echo "local_root=${FTP_ROOT}" >> /etc/vsftpd/vsftpd.conf
	echo "vsftpd_log_file=${LOG_FILE}" >> /etc/vsftpd/vsftpd.conf
	echo "pasv_enable=${PASV_ENABLE}" >> /etc/vsftpd/vsftpd.conf
	echo "pasv_max_port=${PASV_MAX_PORT}" >> /etc/vsftpd/vsftpd.conf
	echo "pasv_min_port=${PASV_MIN_PORT}" >> /etc/vsftpd/vsftpd.conf
}

# Used to run custom commands inside container
if [ ! -z "$1" ]; then
	exec "$@"
else
	init
	add_user
	write_config_file
	/usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf
fi
